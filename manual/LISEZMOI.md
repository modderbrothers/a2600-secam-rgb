![modderbrothers](../assets/product.png)

# Manuel - Mod RGB Atari 2600

[English Version Here](./README.md)

## Outillage requit

Obligatoire pour l'installation du mod :
- Tournevis cruciforme
- Fer à souder + étain
- Perceuse + foret de 15 ou scie cloche de 15 - scie cloche recommandée

## Installation du Mod

#### Précautions générales

- Toutes les manipulations doivent être effectuées hors tension.
- Vous êtes responsables de votre outillage et de ses manipulations. Si vous n'avez pas les aptitudes et/ou l'expérience requise, nous vous conseillons de faire réaliser ces manipulations par une personne qualifiée.

#### Précautions à prendre avant de toucher les cartes et les composants électroniques

Certains composants sont très sensibles à l'électricité statique.
Veillez à vous décharger avant l'ouverture de votre ZX-81 :
- Placez-vous dans une piece en carrelage, et évitez la moquette !
- Évitez les pulls en laine ou autres vêtements qui se chargent facilement !
- Touchez la carcasse métallique d'un appareil électrique pour vous décharger à la terre.

#### Identifier les différentes partie du mod

Lorsqu'on regarde le mod de face, dans le sens ou les textes sont lisibles, nous avons :
- A gauche, une nappe de 25cm qu l'on va aller souder sur la carte mère.
- A droite, une nappe déjà prête dont il faudra fixer le connecteur au boitier.

![parts](assets/identify-parts.jpg)

#### Identifier les broches du connecteur à souder

En regardant le connecteur de face (détrompeur face à nous), en partant du fil le plus à gauche, nous avons :
- Pin 1 : +9V   (Nécessaire à la commutation automatique de la TV)
- Pin 3 : LUM 0 (palette bit 0)
- Pin 5 : LUM 2 (palette bit 2)
- Pin 7 : LUM 1 (palette bit 1)
- Pin 9 : CSYNC (Synchronisation combinée, verticale et horizontale)
- Pin 11: +5V   (Alimentation du mod, VCC sur le TIA)
- Pin 13: Audio (Son des jeux !)
Toutes les pins paires (donc 2, 4, 6, 8, 10, 12, 14) sont des masses (VSS sur la TIA), destinées à isoler les signaux et éviter qu'ils ne se parasitent les uns les autres.

***Ne vous fiez pas aux couleurs des fils ! LEs cables peuvent avoir été montés dans un sans ou dans un autre, ou avec des nappes différentes !***

![soldering](assets/flat-cable-pinout.jpg)

#### Ouverture de la console

Retournez votre console et retirez les 4 vis du boitier, pouis ouvrez votre console en, retirant le capot bas. La carte mère est retenue par les connecteurs à la partie haute.
Ensuite sortez la carte mère délicatement, en faisant attention aux connecteurs et boutons de facade.

Une fois la carte mère de la console sortie, vous allez pouvoir commencer à travailler dessus sans contrainte.

#### Identifier les points de soudures

Pour identifier clairement les points de soudure, il faut préalablement trouver de quel TIA votre Atari 2600 est équipé.
Le TIA, c'est le chip graphique, et il en existe deux séries :
- TIA PAL: CO11903
- TIA NTSC: CO104444
Oui, même sur les versions SECAM, les TIA NTSC ont été utilisés.

![tia](assets/tia-chip.jpg)

Une fois que vous connaissez le modèle de votre TIA, vous pouvez trouver les points de soudure grace au schéma suivant :
![tia](assets/tia-a2600.jpg)

VCC correspond au 5V, et VSS correspond à la masse. ***Ne vous trompez pas !***

Attention également au sens du TIA, qui peut être placé à l'horizontale ou à la verticale !
Identifiez bien la broche 1 (celle à gauche de l'encoche sur le circuit), et n'oubliez pas que lorsque vous le regardez d'en dessous (coté soudure), les cotés gauche/droit sont inversés: La broche 1 est à droite !

#### Souder la nappe souple au TIA

Ce paragraphe est important, lisez-le attentivement.

Pour commencer, intéressons-nous à la longueur des fils de la nappe:
- Le 9V est assez loin du TIA, c'est le fil (le premier à gauche sur le connecteur) que nous allons laisser le plus long.
- Les autres cables devrons être coupés aux longueurs adequate avant d'être dénudés sur 3mm environ.
- Ne coupez aucun fil avant d'avoir positionné correctement votre carte !
- Ne coupez pas tous les fils d'un coup ! Certains nécessiteront quelques mm ou cm de plus que d'autres !
- Ne vous trompez pas de fils ! Vérifiez toujours quel fil vous allez souder. Vérifiez que les masses (les fils pairs) soit toujours en l'air jusqu'à ce que vous arriviez à souder les masses

Commencez par souder le 9V, sans raccourcir le fil.
Soudez ensuite les 3 LUMA (en faisant attention à l'ordre ! Si vous inversez des LUMA, vous n'aurez pas les bonnes couleurs.
Puis soudez la CSYNC.
Et enfin d'Audio, qui est un peu plus loin sur le TIA.
Et en tout dernier les masses et l'alimentation 5V.

Les masses sont un cas spécial. Dans l'absolu, il faut toutes les souder. Mais ce n'est obligatoire et peut se révéler compliqué si vous n'avez pas l'habitude.
Auquel cas, soudez-en simplement 2: les 2 masses aux extrémités (fil 2 et fil 14)

Voici un exemple de soudure sur un TIA PAL:
![example](assets/soldering-example.jpg)

#### Fixer le mod et monter lme connecteur DIN 8

Vous avez fait le plus compliqué.

Il vous reste maintenant à fixer le mod et à monter le connecteur DIN 8 broches sur le boitier.
Voici un example de montage intérieur, dans le bas du boitier :
![montage](assets/mount1.jpg)

Percez simplement le boitier à l'aide d'une scie cloche de 15mm (trouvable facilement en magasin de bricolage ou sur Internet). Cela vous permettra d'avoir un trou net avec peu de bavures.
Positionnez le connecteur à l'intérieur ou à l'extérieur (il vous faudra passer le connecteur 14 broche à travers le trou), puis marquez les trous de fixations.
Percez les trous de fixation, puis procurez-vous vis ou écrous/boulons (recommandé) à la taille de perçage.

Enfin, une fois le connecteur DIN monté, pour éviter que le mod se promène dans la console, fixez le à un endroit ou il ne gêne pas, comme sur les photos suivantes:
![montage](assets/mount2.jpg)

#### Remontez votre console

Procédez dans l'ordre inverse pour remonter votre console. Branchez le connecteur au mod au dernier moment, puis revissez les 4 vis de la console.

Branchez votre cable Péritel et profitez maintenant d'une image aux couleurs vives et nettes !